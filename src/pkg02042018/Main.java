/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg02042018;

import java.util.Scanner;

/**
 *
 * @author Hector
 */

public class Main {
    static void funcion1 (float x){
        if(x<-15){
            System.out.println(x+" se encuentra en el intervalo 1");
        }
        if(x>=-15){
            if(x<=-1){
                System.out.println(x+" se encuentra en el intervalo 2");
            }
        }
        if(x>-1){
            if(x<10){
                System.out.println(x+" se encuentra en el intervalo 3");
            }
        }
        if(x>=10){
            if(x<=25){
                System.out.println(x+" se encuentra en el intervalo 4");
            }
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner valor = new Scanner(System.in);
        System.out.println("¿Cuál es el valor de x?");
        funcion1(valor.nextFloat());
    }
    
}
